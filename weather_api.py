import requests

def get_weather_data(city):
   API_KEY = '22ccb8ad92e5db5c9aae31bc6eda70ae'
   url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API_KEY}'

   try:
      data = requests.get(url).json()  # pentru ca dorim sa ontinem niste date de la url
      # print(data.status_code) # pentru a verifica daca este ok, 200 ok(http request facut cu succes), 404 daca resursa de pe server nu se poate localiza
      # print(data)
      # obtinerea datelor din dict json obtinut la request, dupa key(din aproape in aproape)

      description = data['weather'][0]['description']

      temperature = int(data['main']['temp']) - 273.15

      real_fell = int(data['main']['feels_like']) - 273.15

      print(f'Vremea in: {city}; Detalii vreme: {description}; Temperatura: {round(temperature,2)}; Se simte ca: {round(real_fell,2)}')

   except KeyError as e:
      print(f'A aparut o eroare. Cheia urmatoare nu exista: {e}\n'
            f'Te rog sa verifici URL-ul API')




if __name__ == '__main__':
    city = input('city: ')
    get_weather_data(city)


